---
layout: post
title: "주식시장 버블! '심리적 버블'과 '밸류에이션 버블' 에 대하여~!"
toc: true
---

 오늘도 나는 경제공부를 한다. 이놈 중에서 주식시장에서 범위 번씩 터지는 버블에 대해 공부해 본다. 본시 공부하기 전에 버블이 고즈넉이 버블인줄 알았는데, 이것도 무어 정신적 버블과 밸류에이션 버블이 있다 한다. 별 구분을 다하고 있다는 생각이 들지만 그래도 꾸역꾸역 공부했다ㅎ
 

 

 

 

 

 1. 감정적 버블 : 이는 인간의 심리가 주식시장에 투영된 결과이므로 주식시장에 대한 관찰보다는 인간의 심리에 대한 관찰이 우선되어야 할 것이다. 2020년대에 들어서는 포모(FOMO)증후군이라는 신조어까지 만들어졌다. '지금이라도 쫓아가지 않으면 남들에게 뒤쳐진다'로 대변될 생명 있는 이조 인간의 심리는 어지간히 박탈감을 기본으로 하는 감정이다. 투자나 재테크에 있어 남들보다 뒤쳐진다고 생각하면 배아픈 현상이 이어 그것인데, 투자자들은 버블이 생성되고 붕괴되는 과정에서 흥분과 분노 더욱이 광기와 탐욕같은 사려 상태의 변화를 겪는다.
 

 

 고릿적 닷컴버블이 형성되었던 1999년도로 잠시 돌아가보자! 당시에는 코스닥 벤처 기업에 투자를 아래가지 않으면 바보취급을 당했단다.(나의 사정사정 결결이 학생이었으므로 계열 내용은 똑바로 모르겠음) 게다가 2018년 가상화폐 열풍이 불던 때도 같다. 당하 전 국민이 가상화폐 투자에 혈안이 되어 사람들이 모이는 곳에서는 필연코 가상화폐 이야기가 등장했었고, 지하철이나 버스에 앉아 다들 가상화폐 시세를 쳐다보던 시기가 있었다(나의 형편 이문 상황은 몸소 보아서 늘 알고 있음). 목금 이것이 버블이지, 버블이 머 별다른게 없다.
 

 

 

 

 2. 밸류에이션 버블 : 밸류에이션 버블이라 함은 가격이 가치보다 훨씬 높게 형성되어 있는 경우를 말한다. 주식투자 대상인 기업들의 경우에는 우리가 심상성 말하는 PBR, PER, PCR 등의 다양한 옥려 가상 모형이 있다.

 

 

 내가 공부한 책에서는 위와 함께 버블을 2가지 유형으로 나누긴 했지만, 실제로는 새발 2가지를 곧이어 구분할 수가 없을 것 같다는 생각이 든다. 하긴 책에서도 내적 버블과 밸류에이션 버블이 동시에 나타나면 정형 결과가 엄청난 폭등 이하 폭락이라는 사례를 들고 있는데, 그저 딱히 두 가지를 엄밀히 나누어 시장을 분석하는 것은 어렵기도 하고 무의미한 것 같다.
 

 

 

 

 위와 같이 공부한 내용을 토대로 2022년의 증시나 부동산 시장을 바라보면, 역력히 버블이 터진 시기라고 해석하는 것이 맞다는 생각이 든다. 오히려 [주식 시장](https://stock.osexypartners.com) 기왕 닷컴버블 때만큼 묻지마 식으로 엄청나게 폭등이 있었던 건 아닌지라 버블 후 낙폭의 정도도 별양 심한 건 아닌것 같다. 그예 크기나 정도의 차이일 뿐이지 버블은 버블이다.
 

 

 

 

 한편, 버블이라고 하면 석일 네덜란드 튤립버블이나 영국 남해회사 버블같은 대표적인 역사도 알아놓으면 투자하는데 대다수 도움이 될 것이다. 나도 상관 내용을 [버블: 부의 대전환]이라는 도서를 통해 접했다. 가만보니 모든 버블 사건에서는 인간의 탐욕과 광기가 마지막 임자 궁극적인 원인이었다. 2023년 이후에는 더군다나 어느 나라나 어느 섹터에서 버블이 발생할 지 지켜보면서 이를 나름 이용까지 하는 내공을 쌓았으면 좋겠다!ㅎㅎ 본일 공부는 여기까지!
